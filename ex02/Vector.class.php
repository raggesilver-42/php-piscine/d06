<?php

    require_once dirname(__FILE__) . '/../ex01/Vertex.class.php';

    class Vector
    {
        private $_x;
        private $_y;
        private $_z;
        private $_w = 0;

        public static $verbose = false;

        public function __construct($arr)
        {
            $orig;

            if (isset($arr['dest']) && $arr['dest'] instanceof Vertex)
            {
                if (isset($arr['orig']) && $arr['orig'] instanceof Vertex)
                    $orig = $arr['orig'];
                else
                    $orig = new Vertex(array('x' => 0, 'y' => 0, 'z' => 0));

                $this->_x = $arr['dest']->getX() - $orig->getX();
                $this->_y = $arr['dest']->getY() - $orig->getY();
                $this->_z = $arr['dest']->getZ() - $orig->getZ();
                $this->_w = 0;
            }

            if (Self::$verbose)
                printf("%s constructed\n", $this);
        }

        /**
         * Alternate constructor
         */
        public static function newFromCoords(array $arr): Vector
        {
            return new Vector(array('dest' => new Vertex($arr)));
        }

        public function __destruct()
        {
            if (Self::$verbose)
                printf("%s destructed\n", $this);
        }

        public static function doc()
        {
            return file_get_contents(dirname(__FILE__).'/Vector.doc.txt');
        }

        public function __toString()
        {
            return (sprintf("Vector( x:%0.2f, y:%0.2f, z:%0.2f, w:%0.2f )",
                    $this->_x, $this->_y, $this->_z, $this->_w));
        }

        public function getX() { return $this->_x; }
        public function getY() { return $this->_y; }
        public function getZ() { return $this->_z; }
        public function getW() { return $this->_w; }

        public function magnitude(): float
        {
            return sqrt(
                pow($this->_x, 2) + pow($this->_y, 2) + pow($this->_z, 2)
            );
        }

        public function normalize(): Vector
        {
            $mag = $this->magnitude();

            return Vector::newFromCoords(array(
                'x' => $this->_x / $mag,
                'y' => $this->_y / $mag,
                'z' => $this->_z / $mag
            ));
        }

        public function add(Vector $rhs): Vector
        {
            return Vector::newFromCoords(array(
                'x' => $this->_x + $rhs->getX(),
                'y' => $this->_y + $rhs->getY(),
                'z' => $this->_z + $rhs->getZ()
            ));
        }

        public function sub(Vector $rhs): Vector
        {
            return Vector::newFromCoords(array(
                'x' => $this->_x - $rhs->getX(),
                'y' => $this->_y - $rhs->getY(),
                'z' => $this->_z - $rhs->getZ()
            ));
        }

        public function opposite(): Vector
        {
            return Vector::newFromCoords(array(
                'x' => -$this->_x,
                'y' => -$this->_y,
                'z' => -$this->_z
            ));
        }

        public function scalarProduct($k): Vector
        {
            return Vector::newFromCoords(array(
                'x' => $this->_x * $k,
                'y' => $this->_y * $k,
                'z' => $this->_z * $k
            ));
        }

        public function dotProduct(Vector $v): float
        {
            return (($this->_x * $v->getX()) +
                    ($this->_y * $v->getY()) +
                    ($this->_z * $v->getZ()));
        }

        public function cos(Vector $v): float
        {
            $dot = $this->dotProduct($v);
            $den = $this->magnitude() * $v->magnitude();
            return ($dot / $den);
        }

        public function crossProduct(Vector $v): Vector
        {
            return Vector::newFromCoords(array(
                'x' => ($this->_y * $v->getZ()) - ($this->_z * $v->getY()),
                'y' => -($this->_x * $v->getZ() - ($this->_z * $v->getX())),
                'z' => ($this->_x * $v->getY()) - ($this->_y * $v->getX())
            ));
        }
    }
