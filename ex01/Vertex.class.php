<?php

    require_once dirname(__FILE__) . '/../ex00/Color.class.php';

    class Vertex
    {
        private $_x;
        private $_y;
        private $_z;
        private $_w = 1;
        private $_color;

        public static $verbose = false;

        public function __construct(array $arr)
        {
            $this->_x = $arr['x'];
            $this->_y = $arr['y'];
            $this->_z = $arr['z'];
            if (isset($arr['w']))
                $this->_w = $arr['w'];
            if (isset($arr['color']) && $arr['color'] instanceof Color)
                $this->_color = $arr['color'];
            else
                $this->_color = new Color(array('red'   => 255,
                                                'green' => 255,
                                                'blue'  => 255));

            if (Self::$verbose)
                printf("%s constructed\n", $this);
        }

        public function __destruct()
        {
            if (Self::$verbose)
                printf("%s destructed\n", $this);
        }

        public function __toString(): string
        {
            $fmt;

            $arr = array($this->_x, $this->_y, $this->_z, $this->_w);
            if (Vertex::$verbose)
            {
                $fmt = "Vertex( x: %0.2f, y: %0.2f, z:%0.2f, w:%0.2f, %s )";
                array_push($arr, $this->_color);
            }
            else
                $fmt = "Vertex( x: %0.2f, y: %0.2f, z:%0.2f, w:%0.2f )";

            return (vsprintf($fmt, $arr));
        }

        public static function doc()
        {
            printf("%s",
                file_get_contents(dirname(__FILE__) . '/Vertex.doc.txt')
            );
        }

        public function getX() { return $this->_x; }
        public function getY() { return $this->_y; }
        public function getZ() { return $this->_z; }
        public function getW() { return $this->_w; }
        public function getColor(): Color { return $this->_color; }

        public function setX($val) { $this->_x = $val; }
        public function setY($val) { $this->_y = $val; }
        public function setZ($val) { $this->_z = $val; }
        public function setW($val) { $this->_w = $val; }
        public function setColor(Color $val) { $this->_color = $val; }
    }
