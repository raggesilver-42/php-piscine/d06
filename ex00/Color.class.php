<?php

    class Color
    {
        public static $verbose = false;

        public $red   = 0;
        public $green = 0;
        public $blue  = 0;

        function __construct(array $arr)
        {
            if (isset($arr['rgb']))
            {
                $this->red   = ($arr['rgb'] >> 16) & 0xFF;
                $this->green = ($arr['rgb'] >> 8) & 0xFF;
                $this->blue  = ($arr['rgb'] >> 0) & 0xFF;
            }
            else if (isset($arr['red']) && isset($arr['green'])
                     && isset($arr['blue']))
            {
                $this->red   = intval($arr['red']);
                $this->green = intval($arr['green']);
                $this->blue  = intval($arr['blue']);
            }

            if (Self::$verbose)
                printf("%s constructed.\n", $this);
        }

        function __destruct()
        {
            if (Self::$verbose)
                printf("%s destructed.\n", $this);
        }

        public function add(Color $toAdd): Color
        {
            $res = new Color(array('red'   => $this->red + $toAdd->red,
                                   'green' => $this->green + $toAdd->green,
                                   'blue'  => $this->blue + $toAdd->blue));

            return ($res);
        }

        public function sub(Color $toSub): Color
        {
            $res = new Color(array('red'   => $this->red - $toSub->red,
                                   'green' => $this->green - $toSub->green,
                                   'blue'  => $this->blue - $toSub->blue));

            return ($res);
        }

        public function mult($toMult): Color
        {
            $res;

            if ($toMult instanceof Color)
                $res = new Color(array('red'   => $this->red * $toMult->red,
                                       'green' => $this->green * $toMult->green,
                                       'blue'  => $this->blue * $toMult->blue));
            else
                $res = new Color(array('red'   => $this->red * $toMult,
                                       'green' => $this->green * $toMult,
                                       'blue'  => $this->blue * $toMult));

            return ($res);
        }

        public function __toString(): string
        {
            $fmt = "Color( red: %3.3s, green: %3.3s, blue: %3.3s )";
            return (sprintf($fmt, $this->red, $this->green, $this->blue));
        }

        public static function doc()
        {
            printf("%s", file_get_contents(dirname(__FILE__).'/Color.doc.txt'));
        }
    }
